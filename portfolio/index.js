var texto = document.getElementById("textboxh");
var boton = document.getElementById("buttonh");
boton.addEventListener("click", clicknDraw);

// Canvas definition
var d = document.getElementById("eiffel");
var canvas = d.getContext("2d");
var ancho = d.width;

// Functions

function drawLine(color, initialX, initalY, finalX, finalY){
    canvas.beginPath();          // Path opening.
    canvas.strokeStyle = color;  // Stroke style definition.
    canvas.moveTo(initialX , initalY);    // Stroke start.
    canvas.lineTo(finalX , finalY);    // Stroke finished.
    canvas.stroke();             // Stroke application.
    canvas.closePath();           // Path closing.
}

function clicknDraw(){
    var lines = parseInt(texto.value);
    var l = 0;
    var iy, fx;
    var colorStyle = "#521751";

    for (l = 0; l < lines; l++)
    {
        iy = d.width / texto.value * l;
        fx = d.width / texto.value * (l + 1);
        drawLine(colorStyle , 1 , iy, fx, 299);
        console.log(l)
    }
    // Eiffel borders
    drawLine(colorStyle , 1 , 1, 1, d.width -1);
    drawLine(colorStyle , 1 , d.width -1, d.width - 1, d.width - 1);
}

console.log(navigator);